from django.db import models


# Create your models here.

class employee(models.Model):
    sex_types = (
        ('M', 'Male'),
        ('F', 'Female'),
        ('O', 'Other'),
    )

    id = models.AutoField(primary_key=True)
    full_name = models.CharField(max_length=100)
    sex = models.CharField(max_length=1, choices=sex_types)
    birthdate = models.DateField()
    Nic = models.CharField(max_length=20)
    Phone_number = models.CharField(max_length=20)
    name2 = models.CharField(verbose_name='Emergerncy contact person', max_length=50)
    phone_number2 = models.CharField(verbose_name='emergency contact number', max_length=20)
    Address = models.CharField(max_length=150)

    def __str__(self):
        return self.full_name

class job(models.Model):
    designation = models.CharField(max_length=50)
    Nic = models.CharField(max_length=20)
    file_no = models.CharField(max_length=20)
    first_appointment = models.CharField(max_length=50)
    grade_II_promotion = models.CharField(max_length=20)
    grade_I_promotion = models.CharField(max_length=20)
    high_promotion = models.CharField(max_length=20)
    special_grade_promotion = models.CharField(max_length=20)
    widow_and_ophan_no = models.CharField(max_length=20)
    salary_increment_date = models.CharField(max_length=20)
    appointment_date_hospital = models.DateField(verbose_name='appointment date to this hospital')
    pre_service_location = models.CharField(max_length=50)

    def __str__(self):
        return self.designation


class leaves(models.Model):
    Nic = models.CharField(max_length=20)
    Casual_leave = models.DateField()
    Resting_leave = models.DateField()

    def __str__(self):
        return self.Nic
