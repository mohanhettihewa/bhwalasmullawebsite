from django.http import HttpResponse
from django.template import loader
from django.shortcuts import render
from employee.models import employee
from django.core import serializers


def index(request):
    template = loader.get_template('employee/index.html')
    return render(request, 'employee/index.html')


def grid(request):
    employee_list = employee.objects.all()
    employee_list1 = serializers.serialize('json', employee_list)

    context = {
        'employee_data': employee_list
    }

    conitext = {'first_name': 'John', 'last_name': 'Doe'}
    # return render(request,'employee/index.html', context)
    return HttpResponse(employee_list1, content_type="text/json-comment-filtered")
