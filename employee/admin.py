from django.contrib import admin

# Register your models here.
from employee.models import  employee,job,leaves
admin.site.register(employee)
admin.site.register(job)
admin.site.register(leaves)
