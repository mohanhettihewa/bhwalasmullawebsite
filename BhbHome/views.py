from django.http import HttpResponse,HttpResponseRedirect,Http404
from django.template import loader
from  django.template import  loader
from django.shortcuts import render
from django.core.mail import BadHeaderError,send_mail
from .models import  Email
from .forms import EmailForm
from django.http import JsonResponse
from django.core import serializers
import json


def index(request):


    return render(request,'BhbHome/index.html')


def about(request):


    return  render(request,'BhbHome/About.html')


def contactus(request):

    return render(request,'BhbHome/contactus.html')

def visionmission(request):

    return render(request,'BhbHome/VisionMission.html')

def News(request):

    return render(request,'BhbHome/News.html')


def Gallery(request):

    return render(request,'BhbHome/Gallery.html')


def send_email(request):
    subject = request.POST.get('email_subject', '')
    message = request.POST.get('first_name', '')
    from_email = request.POST.get('email', '')
    if subject and message and from_email:
        try:
            send_mail(subject, message, from_email, ['mohanhettihewa@gmsil.com'])
        except BadHeaderError:
            return HttpResponse('Invalid header found.')
        return HttpResponseRedirect('/contact/thanks/')
    else:
        # In reality we'd use a form class
        # to get proper validation errors.
        return HttpResponse('Make sure all fields are entered and valid.')



def sendemail1(request):
        if request.is_ajax() :
            subject = request.POST.get('content[subject]')
            message = request.POST.get('content[email_body]')
            email = request.POST.get('content[email]')
            name=request.POST.get('content[first_name]')
            data1={
                "Subject":subject,
                "body":message,
                "email":email,
                "name":name,
            }
            send_mail(
               subject,
                 message,
                 email,
                 ['bhbwalasmullaweb@gmail.com'],

             )

            data=json.dumps(data1)
            return HttpResponse(data,content_type='application/json')
            # return render(request,'BhbHome/contactus.html',{'name':name})
        else:
            raise Http404
