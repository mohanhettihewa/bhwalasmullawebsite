from django.urls import path
from . import  views



urlpatterns =[


    path('',views.index,name='index'),
    path('about',views.about,name='about_us'),
    path('contactus',views.contactus, name='contact_us'),
    path('sendemail1',views.sendemail1,name='send_email'),
    path('email', views.send_mail, name='email'),
    path('vision',views.visionmission,name='visionmission'),
    path('vision',views.News,name='news'),
    path('vision',views.Gallery,name='gallery')
]