from django.db import models

# Create your models here.
# Create your models here.
class Email(models.Model):
    # NICK NAME should be unique
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    email_subject=models.CharField(max_length=150)
    email_body=models.CharField(max_length=250)
    email=models.CharField(max_length=100)

