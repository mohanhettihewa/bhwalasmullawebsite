from django.db import models

class event(models.Model):

    Date = models.DateField(auto_now=True)
    Title = models.CharField(max_length=100)
    Body = models.CharField(max_length=250)
    img = models.ImageField(upload_to='static/event')

