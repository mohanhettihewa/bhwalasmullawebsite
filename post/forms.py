from django import forms


class EventForm(forms.Form):

    Title = forms.CharField(max_length=100)
    Body = forms.CharField(max_length=250)

